require mrfioc2,2.7.13-ESS0
require pev,0.1.2
require iocStats,3.1+


################### Configuration Timing ###################
epicsEnvSet("SYSEVG"          "ISrc-010:TS")
epicsEnvSet("EVENT_14HZ"      "14")

################### Configuration EVG ###################
epicsEnvSet("EVG"             "EVG-01")
epicsEnvSet("EVG_VMESLOT"     "2")

mrmEvgSetupVME($(EVG), $(EVG_VMESLOT), 0x100000, 1, 0x01)

dbLoadRecords("evg-vme-230.db", "DEVICE=$(EVG), SYS=$(SYSEVG), EvtClk-FracSynFreq-SP=88.0525, TrigEvt0-EvtCode-SP=$(EVENT_14HZ), Mxc1-Frequency-SP=14, Mxc1-TrigSrc0-SP=1")

mrmEvgSoftTime("$(EVG)")

################### Configuration EVR ###################
epicsEnvSet("SYSEVR"          "ISrc-010:ISS")
epicsEnvSet("EVR"             "EVR-Magtr")
epicsEnvSet("EVR_VMESLOT"     "5")

mrmEvrSetupVME($(EVR), $(EVR_VMESLOT), 0x3000000, 5, 0x026)

dbLoadRecords("evr-vme-230.db", "DEVICE=$(EVR), SYS=$(SYSEVR), Link-Clk-SP=88.0525, FrontOut0-Src-SP=0, FrontOut0-Ena-SP=1, FrontUnivOut0-Src-SP=0, FrontUnivOut0-Ena-SP=1, Pul0-Prescaler-SP=77, Pul0-Width-SP=20000, Pul0-Delay-SP=0, Pul1-Prescaler-SP=77, FrontOut1-Src-SP=1, FrontOut1-Ena-SP=1, Pul1-Prescaler-SP=77, Pul1-Width-SP=10000, Pul1-Delay-SP=0")

# Pulser dedicated to the Magnetron
dbLoadRecords("evr-pulserMap.template", "DEVICE=$(EVR), SYS=$(SYSEVR), EVT=$(EVENT_14HZ), PID=0, F=Trig, ID=0")

# Pulser dedicated to the Chopper
dbLoadRecords("evr-pulserMap.template", "DEVICE=$(EVR), SYS=$(SYSEVR), EVT=$(EVENT_14HZ), PID=1, F=Trig, ID=1")

dbLoadRecords("evr-softEvent.template", "DEVICE=$(EVR), SYS=$(SYSEVR), EVT=$(EVENT_14HZ), CODE=$(EVENT_14HZ)")


################### iocStats ###################
dbLoadTemplate(iocAdminSoft.substitutions,IOC="ISrc-010:Ctrl-IOC-TS")

 
iocInit


########## TIMING GENERATOR: timestamp synchronisation ##############

dbpf $(SYSEVR)-$(EVR):Time-I.EVNT 14
dbpf $(SYSEVR)-$(EVR):Time-I.TSEL $(SYSEVR)-$(EVR):Event-14-Cnt-I.TIME

sleep(5)
dbpf $(SYSEVG)-$(EVG):SyncTimestamp-Cmd 1


###########################################################
###Operation Parameters from ICSHWI-1265
###########################################################

dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.DRVH 14
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.ADEL 1
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.PREC 3
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.EGU "Hz"
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-RB.EGU "Hz"
dbpf ISrc-010:ISS-EVR-Magtr:Pul0-Width-SP.DRVH 8000
dbpf ISrc-010:ISS-EVR-Magtr:Pul0-Width-SP.HIHI 8000
dbpf ISrc-010:ISS-EVR-Magtr:Pul0-Width-SP.HIGH 6000
dbpf ISrc-010:ISS-EVR-Magtr:Pul0-Width-SP.PREC 1
dbpf ISrc-010:ISS-EVR-Magtr:Pul0-Width-SP.EGU "ms"
dbpf ISrc-010:ISS-EVR-Magtr:Pul0-Width-SP.HHSV "MAJOR"
dbpf ISrc-010:ISS-EVR-Magtr:Pul0-Width-SP.HSV "MINOR"
dbpf ISrc-010:ISS-EVR-Magtr:Pul0-Width-RB.HIGH 6010
dbpf ISrc-010:ISS-EVR-Magtr:Pul0-Width-RB.EGU "us"
dbpf ISrc-010:ISS-EVR-Magtr:Pul0-Width-RB.HSV "MINOR"
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.DRVH 20
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.HIHI 20
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.HIGH 14.1
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.ADEL 1
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.PREC 1
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.EGU "Hz"
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.HHSV "MAJOR"
dbpf ISrc-010:TS-EVG-01:Mxc1-Frequency-SP.HSV "MINOR"
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Width-SP.DRVH 8000
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Width-SP.HIHI 8000
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Width-SP.HIGH 6000
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Width-SP.PREC 1
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Width-SP.EGU "ms"
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Width-SP.HHSV "MAJOR"
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Width-SP.HSV "MINOR"
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Delay-SP.DRVH 8000
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Delay-SP.HIHI 8000
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Delay-SP.HIGH 6000
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Delay-SP.PREC 1
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Delay-SP.EGU "ms"
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Delay-SP.HHSV "MAJOR"
dbpf ISrc-010:ISS-EVR-Magtr:Pul1-Delay-SP.HSV "MINOR"

